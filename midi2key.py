import ctypes
import pygame
import pygame.midi as midi
import configparser as cp
from pywinauto.win32functions import SendInput
import time, sys

"""Author: panu.lindqvist"""

##################################
# https://stackoverflow.com/questions/11906925/python-simulate-keydown 
# Stripped version of example
##################################
LONG = ctypes.c_long
DWORD = ctypes.c_ulong
ULONG_PTR = ctypes.POINTER(DWORD)
WORD = ctypes.c_ushort
   
class MOUSEINPUT(ctypes.Structure):
    _fields_ = (('dx', LONG),
                ('dy', LONG),
                ('mouseData', DWORD),
                ('dwFlags', DWORD),
                ('time', DWORD),
                ('dwExtraInfo', ULONG_PTR))
                
class KEYBDINPUT(ctypes.Structure):
    _fields_ = (('wVk', WORD),
                ('wScan', WORD),
                ('dwFlags', DWORD),
                ('time', DWORD),
                ('dwExtraInfo', ULONG_PTR))
                
class HARDWAREINPUT(ctypes.Structure):
    _fields_ = (('uMsg', DWORD),
                ('wParamL', WORD),
                ('wParamH', WORD))
                
class _INPUTunion(ctypes.Union):
    _fields_ = (('mi', MOUSEINPUT),
                ('ki', KEYBDINPUT),
                ('hi', HARDWAREINPUT))
                
class INPUT(ctypes.Structure):
    _fields_ = (('type', DWORD),
                ('union', _INPUTunion))
                
def SendInput(*inputs):
    nInputs = len(inputs)
    LPINPUT = INPUT * nInputs
    pInputs = LPINPUT(*inputs)
    cbSize = ctypes.c_int(ctypes.sizeof(INPUT))
    return ctypes.windll.user32.SendInput(nInputs, pInputs, cbSize)

INPUT_MOUSE = 0
INPUT_KEYBOARD = 1
INPUT_HARDWARE = 2

def Input(structure):
    if isinstance(structure, MOUSEINPUT):
        return INPUT(INPUT_MOUSE, _INPUTunion(mi=structure))
    if isinstance(structure, KEYBDINPUT):
        return INPUT(INPUT_KEYBOARD, _INPUTunion(ki=structure))
    if isinstance(structure, HARDWAREINPUT):
        return INPUT(INPUT_HARDWARE, _INPUTunion(hi=structure))
    raise TypeError('Cannot create INPUT structure!')

KEYEVENTF_EXTENDEDKEY = 0x0001
KEYEVENTF_KEYUP = 0x0002
KEYEVENTF_SCANCODE = 0x0008
KEYEVENTF_UNICODE = 0x0004

def KeybdInput(code, flags):
    return KEYBDINPUT(code, code, flags, 0, None)

def Keyboard(code, flags=0):
    return Input(KeybdInput(code, flags))



##############################
# https://github.com/xamox/pygame/blob/master/examples/midi.py 
# Deconstructed version of example
##############################

def midi_poll(input_id):
  if input_id.poll():
    midi_events = input_id.read(10)
    # convert them into pygame events.
    return pygame.midi.midis2events(midi_events, input_id.device_id)
  else:
    return None


def _print_device_info():
  for i in range(pygame.midi.get_count()):
    r = pygame.midi.get_device_info(i)
    (interf, name, input, output, opened) = r

    in_out = ""
    if input:
      in_out = "(input)"
    if output:
      in_out = "(output)"

    print(
      "%2i: interface :%s:, name :%s:, opened :%s:  %s"
      % (i, interf, name, opened, in_out)
    )


def pygame_init():
  pygame.init()
  pygame.fastevent.init()

  pygame.midi.init()

  _print_device_info()

  device_id = request_device()

  if device_id is None:
    input_id = pygame.midi.get_default_input_id()
  else:
    input_id = device_id

  return pygame.midi.Input(input_id)


####################
#  panu.lindqvist
####################

MODE_NORMAL = 1
MODE_SETUP = 2


def config_reader(filename):
  try:
    config = cp.ConfigParser(inline_comment_prefixes=(';'))
    if not config.read(filename):
      print("Can't read config.ini")
      sys.exit()
    return config
  except cp.Error as e:
    print(e)
    return 


def config_get(section, key):
  try:
    return CONF[section][key]
  except KeyError as ke:
    # No matching key
    if MODE == MODE_NORMAL:
      print(ke)
    return None
  except Exception as e:
    print(e)
    return None


def send_keystroke(keys):
  keys = keys.split(SEP)
  print(keys)

  for key in keys:
    # Keys down 1 at the time
    if key == '':
      # Allow buttons without keymapping to exist in config.ini
      return
    elif key not in CONF['KEYS']:
      raise Exception("Bad config.ini, key not recognized")
    else:
      _i = CONF['KEYS'][key].strip()
      _hex = int(_i, 16)
      SendInput(Keyboard(_hex))
      time.sleep(0.1)

  for key in reversed(keys):
    # Key up in inverse order
    _i = CONF['KEYS'][key].strip()
    _hex = int(_i, 16)
    SendInput(Keyboard(_hex, KEYEVENTF_KEYUP))
    time.sleep(0.1)


def request_device():
  return int(input("Select device: "))


def request_game_conf():
  return str(input("Name of configs you want to load. (example: 'IL2'): "))


def request_running_mode():
  return int(input("""
    %s - Run midi2key\n
    %s - Run midi2Key setup mode\n
    Select mode: 
    """ % (MODE_NORMAL, MODE_SETUP)))


def print_game_configs():
  print("Select profile from configs listed below: ")
  sections = CONF.sections()
  for i in range(len(sections)):
    print('- ' + sections[i])


def main():
  """ Start pygame and begin polling for midi events.
  Compares midi events to config.ini and sends if possible
  the corresponding keystrokes """
  input_id = pygame_init()
  pygame.display.set_mode((1, 1))

  try:
    while True:    
      if events := midi_poll(input_id):
        for event in events:
          if MODE == MODE_NORMAL:
            if((keys := config_get(PROFILE, PFIX + str(event.data1))) != None and event.data2 != 0):
              send_keystroke(keys)
          elif MODE == MODE_SETUP:
            if(event.data2 != 0):
              value = config_get(PROFILE, PFIX + str(event.data1))
              print(
                  PFIX 
                  + str(event.data1) 
                  + ' : ' 
                  + (str(value.split(SEP)) if (value != None and value != '') else 'not assigned'))
  except Exception as e:
    print(e)
  except KeyboardInterrupt:
    print("User KeyboardInterrupt, quitting..")
  finally:
    del input_id
    pygame.midi.quit()
    sys.exit()


if __name__ == "__main__":
  CONF = config_reader("config.ini")
  MODE = request_running_mode()
  print_game_configs()
  PROFILE = request_game_conf()
  SEP = config_get("SETTINGS", "SEPARATOR")
  PFIX = config_get("SETTINGS", "MIDI_BTN_PREFIX")
  main()