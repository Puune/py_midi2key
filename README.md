# Midi2Key for Launcpad Mini -> IL-2
Made to send keystrokes into DirectX apps, like IL-2. Uses ctypes, pygame, configparserm, pywinauto and system libraries

## Instructions  
04.11.2021  
Made with Novation Launchpad Mini as controller, can probably be used with other button-controllers as well.   
Made using Python 3.9.7 64-bit and libraries:
- ctypes
- pygame
- configparser as cp
- pywinauto.win32functions import SendInput
- time, sys
  
### Disclaimer
So much is copied from stack overflow.
  
### Sources
- https://stackoverflow.com/questions/11906925/python-simulate-keydown
- https://github.com/xamox/pygame/blob/master/examples/midi.py  
